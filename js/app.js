$(document).ready(function() {

  //CARGAR MODULOS

  $("header").load("_header.html", function(){
    // MENÚ ACTIVO
    switch(true){
      case $('main').hasClass('page-projects'): $('header .nav a').removeClass('is-active'); $("header .nav a[data-section='proyectos']").addClass('is-active'); break;
      case $('main').hasClass('page-project-detail'): $('header .nav a').removeClass('is-active'); $("header .nav a[data-section='proyectos']").addClass('is-active'); break;
      case $('main').hasClass('page-who'): $('header .nav a').removeClass('is-active'); $("header .nav a[data-section='somos']").addClass('is-active'); break;
      case $('main').hasClass('page-blog'): $('header .nav a').removeClass('is-active'); $("header .nav a[data-section='blog']").addClass('is-active'); break;
      case $('main').hasClass('page-blog-detail'): $('header .nav a').removeClass('is-active'); $("header .nav a[data-section='blog']").addClass('is-active'); break;
      case $('main').hasClass('page-pub'): $('header .nav a').removeClass('is-active'); $("header .nav a[data-section='publicaciones']").addClass('is-active'); break;
      case $('main').hasClass('page-contact'): $('header .nav a').removeClass('is-active'); $("header .nav a[data-section='contacto']").addClass('is-active'); break;
    }
  });
  $("#mb-menu").load("_mobile.html");
  $("footer").load("_footer.html");
  
  // BOTÓN REGRESAR ARRIBA
  $('body').on('click', '.scroll-up', function(){ 
    $('html, body').stop().animate({ scrollTop: 0 }, 1000); 
  }); 

});

//HOVER EN BLOG

if ($('.post').length > 0) {

  $(".post").on('mouseover', function() {
    $('.post').not(this).addClass('off');
  });

  $(".post").on('mouseout', function() {
    $('.post').not(this).removeClass('off');
  });

}

//SLIDER INIT POR SLIDER PRESENTE

if ($('.pub-swiper').length > 0) {

  $(".modal").on('show.bs.modal', function() {
    setTimeout(function() {

      var mySwiper = new Swiper('.pub-swiper .swiper-container', {
        loop: true,
        direction: 'horizontal',
        effect: 'slide',
        slidesPerView: '1',
        keyboard: {
          enabled: true,
          onlyInViewport: true,
        },
        pagination: {
          el: '.swiper-pagination',
          type: 'fraction'
        },
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },
        autoplay: {
          delay: 4000,
          reverseDirection: true,
        },
      })

    }, 300);
  });

}

// LOADER

$(window).on('load', function() {
  $('.loader-image').fadeOut(350);
  $('.loader-component').delay(600).fadeOut('slow');
  $('main').css({
    opacity: 1
  });
});

// BOTON MOVIL

$('body').on('click', '.mobile-btn', function() {
  // $(".mobile-btn").toggleClass('open');
  $(".mobile-menu").toggleClass('open');
  $("body").toggleClass('body-no-scroll');
});


// AGREGAR EFECTOS DE ON SCROLL

$(window).on('load', function() {
  $('.element').attr('data-aos', 'fade-in-up');
  AOS.init();
});